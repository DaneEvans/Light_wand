#!/bin/bash

# navigate to this and execute using "sudo bash install.sh"
# designed for Raspbian Jessie Lite 29/9/2017

#clear

echo "updating apt-get"
sudo apt-get update
sudo apt-get upgrade

echo "installing new software"
sudo apt-get install build-essential python-dev python-imaging-tk git scons swig

# make directories for the whole app

echo "making new Desktop"
sudo mkdir ~/Desktop
cd Desktop

sudo mkdir light_wand
cd light_wand

sudo mkdir src
cd src

# Set up the WS2812 libraries

echo "building new WS281X libraries"
sudo git clone https://github.com/jgarff/rpi_ws281x.git
cd rpi_ws281x
sudo scons

cd python
sudo python setup.py install

cd ~/Desktop/src

#git pull the Image wand library
# navigate to it
sudo git clone https://gitlab.com/DaneEvans/Light_wand.git ## use https so you don't need to set up keys or anything

##sudo python ~/Desktop/LightPainter/LBautorun.py


# auto login
sudo echo "set up for auto login. options B1 > B2"
sudo raspi-config

# autorun
sudo echo "setting up auto run"
echo " add the following lines in here 
cd ~/Desktop/LightPainter/LBautorun.py
sudo python LBautorun.py & "

sudo nano /etc/profile

###
# directory structure
# /home/pi/Desktop/lightwand
#							/WIP 				   --- this is where the current code will go. Priority 2
#							/release			   --- this is where the release code will be  Priority 3
# 							/src
#								/rpi_ws281x        --- contains the git repo for the WS2812 LEDs
#								/Light_wand        --- contains the git repo for the driver function (this will be a fall back program - priority 4)
#
# /media/sdaX/lightwand                            --- this may contains new code  (Priority 1) 
#						/Images					   --- this may contain new images
#
#
#
##