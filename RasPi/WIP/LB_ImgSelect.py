import PIL.Image as Image
#import Image


def loadImage(filename):
    """ loadImage(filename)
    filename includes the director from the current running directory.
    """
    img = Image.open(filename)
    if img.mode != 'RGB':
        img = img.convert()
    pixels = img.load()
    width = img.size[0]
    height = img.size[1]
    
    return(img,pixels,width,height)
    
