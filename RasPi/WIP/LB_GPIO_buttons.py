import RPi.GPIO as GPIO
import LB_WS2812_output as display

# general fixed defines 
LED_PIN        = 18      # GPIO pin connected to the pixels (must support PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 5       # DMA channel to use for generating signal (try 5)
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)

# Button GPIO port defines
menu =   21	# pause
up =     22	# change delay
down =   23	# change column display speed
left =   24	# cycle dimness
right =  26	# next image

# interruptType
(Int_none, Int_NextIm, Int_Dim,Int_scroll, Int_interframe, Int_pause) = range(6)

GPIOlist = [menu,up, down, left, right]
	
def InitGPIO():
	GPIO.setmode(GPIO.BOARD)
	
	GPIO.setup(GPIOlist, GPIO.IN, pull_up_down=GPIO.PUD_UP)
	
	# Can't do these as a list
	GPIO.add_event_detect(menu,GPIO.FALLING,bouncetime=200)
	GPIO.add_event_detect(up,GPIO.FALLING,bouncetime=200)
	GPIO.add_event_detect(down,GPIO.FALLING,bouncetime=200)
	GPIO.add_event_detect(left,GPIO.FALLING,bouncetime=200)
	GPIO.add_event_detect(right,GPIO.FALLING,bouncetime=200)

	return 0
	
def cleanup():
	GPIO.cleanup(GPIOlist)
	
def checkInterrupts():
	interrupted = False
	interruptType = Int_none
			
	if GPIO.event_detected(menu):  # pause
		print "pause detected"
		interrupted = True
		interruptType = Int_pause
					
	if GPIO.event_detected(up):       # change interimage space
		print "inter-frame changed"
		interrupted = True
		interruptType = Int_interframe

	if GPIO.event_detected(down):     # change scroll speed
		print "changed scroll speed"
		interrupted = True
		interruptType = Int_scroll
		
	
	if GPIO.event_detected(right):		# next image
		print " next image"
		interrupted = True
		interruptType = Int_NextIm
	
	if GPIO.event_detected(left):   #dimness. can be changed without starting the image over
		print "change brightness"
		interrupted = True
		interruptType = Int_Dim
		
	return interrupted, interruptType 

def getrepeatT(arg):
    switcher = {
		0: 0,
		1: 0.1,
		2: 0.2,
		3: 0.5,
		4: 1,
		5: 2
    }
    return switcher.get(arg%len(switcher))
	
def getscrollT(arg):
    switcher = {
		0: 0.001,
		1: 0.005,
		2: 0.01,
		3: 0.02,
		4: 0.05,
		5: 0.1
    }
    return switcher.get(arg%len(switcher))
	
def getDimness(arg):
    switcher = {
		0: 10,
		1: 25,
		2: 50,
		3: 100,
		4: 200,
		5: 255
    }
    return switcher.get(arg%len(switcher))