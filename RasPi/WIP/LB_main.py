#!/usr/bin/python
 
## Light wand code for WS2812b LED's on a RPi version 1B?
 
import RPi.GPIO as GPIO,  time, os
try: 
	import Image ## PIL 
except: 
	from PIL import Image ## Pillow

from neopixel import *

# my modules
from LB_Config import *
import LB_ImgSelect as Pics
import LB_WS2812_output as display
import LB_GPIO_buttons as buttons
		

		
# Fixed things
image = 0


Filelist = os.listdir(imageDir)
Imagelist = [img for img in Filelist if (img.endswith('.jpg') or img.endswith('.gif') or img.endswith('.png')) and not img.startswith('.')]

filename = imageDir + Imagelist[image]


		 
# Load image
print "Loading..."
(img,pixels,width,height) = Pics.loadImage(filename)

print "%dx%d pixels" % img.size
# To do: add resize here if image is not desired height

def renderImage (strip, imagePix,width,height, scrollT):
	for i in range(width):
		display.printBar(strip,imagePix, i,height)
		keypress = buttons.checkInterrupts()
		if keypress[0]:
			display.clearBar(strip, height)
			return keypress
		time.sleep(scrollT)
	return 0,0	

buttons.InitGPIO() 
	  
# Initiate the strip.  
strip = Adafruit_NeoPixel(LED_COUNT, buttons.LED_PIN, buttons.LED_FREQ_HZ, buttons.LED_DMA, buttons.LED_INVERT, buttons.getDimness(dimState))
strip.begin() 
	
print "Displaying..."

while True:   ## MAIN CODE  

	time.sleep(buttons.getrepeatT(repeatTstate))
	
	keypress = renderImage (strip,pixels, width, height, buttons.getscrollT(scrollTstate))
	
	#### ##########    ISR    #########################
	# ------   for interrupts within the image rendering
	if keypress[0]:
		if (keypress[1] == buttons.Int_scroll):
			scrollTstate +=1
		if (keypress[1] == buttons.Int_interframe):
			repeatTstate +=1
		if (keypress[1] == buttons.Int_Dim):
			dimState +=1
			LED_BRIGHTNESS = buttons.getDimness(dimState)
			print LED_BRIGHTNESS
			strip = Adafruit_NeoPixel(LED_COUNT, buttons.LED_PIN, buttons.LED_FREQ_HZ, buttons.LED_DMA, buttons.LED_INVERT, LED_BRIGHTNESS)
			strip.begin()
		if (keypress[1] == buttons.Int_pause):
			pauseFlag = True
			quitcount=0
			while pauseFlag:
				if not(GPIO.input(buttons.menu)):
					quitcount +=1
					if quitcount > 40:
						os.system('sudo shutdown now') 
				display.clearBar(strip, height)
				if GPIO.event_detected(buttons.menu):
					pauseFlag = False
				time.sleep(0.1)
		if (keypress[1] == buttons.Int_NextIm):

			image += 1
			image = image % len(Imagelist)
			
			filename = imageDir + Imagelist[image]
			(img,pixels,width,height) = Pics.loadImage(filename)			


			while (type(pixels[1,1]) != type((1,1))): # While != tuple
 				image += 1
				image = image % len(Imagelist)
			
				filename = imageDir + Imagelist[image]
				(img,pixels,width,height) = Pics.loadImage(filename)
				#print type(pixels[1,1])
				
				
			print 'this image: ', image, Imagelist[image]
			print type(pixels[1,1])
	