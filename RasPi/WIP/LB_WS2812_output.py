# import PiGPIO_init as init

import RPi.GPIO as GPIO
from neopixel import *

import time
from LB_Config import *


# Set the numbers of the channels in the input image
R = 0
G = 1
B = 2

# Calculate gamma correction table.  
#Need to have a look at a few images with and without this. it is slow to start, but larger differences near the top end... 
gamma = bytearray(256)
for i in range(256):
	  gamma[i] = int(pow(float(i) / 255.0, 2.5) * 127.0 + 0.5)

# ------ fns -------------------------
	  
def clearBar (strip, LEDs):
	for i in range(LEDs):
		strip.setPixelColor(i,Color(0,0,0))
	strip.show()

def printBar (strip, imagePix, column, height):
	for i in range(height):
		#strip.setPixelColor(i,Color(imagePix[column,i][R],imagePix[column,i][G],imagePix[column,i][B]))
		strip.setPixelColor(i,Color(gamma[imagePix[column,i][R]],gamma[imagePix[column,i][G]],gamma[imagePix[column,i][B]]))
	strip.show()
	return 0
	
	
	
