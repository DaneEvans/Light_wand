import os
import shutil
import datetime

def autorun(testDir, defaultDir, programName):
	
	try:
		filelist = os.listdir(testDir)
		if (programName in filelist):
			print '  There is a copy of ', programName, ' in ', testDir
			return 0
		else:
			print '  No updated copy, using default ', programName, ' in ', defaultDir
			return 1
	except:
		print '  No updated copy, using default ', programName, ' in ', defaultDir
		return 2


def updateFiles(sourceDir, destDir):
	sourceFiles = os.listdir(sourceDir)
	destFiles  =  os.listdir(destDir)
	if (len(destFiles)!= len(sourceFiles)):
		for i in range(len(sourceFiles)):
			if not(sourceFiles[i] in destFiles):
				shutil.copy(sourceDir+sourceFiles[i], defaultDir+sourceFiles[i])


if __name__ =='__main__':
	usbCD = '/LightPainter/'
	defaultDir = '/home/pi/Desktop/LightPainter/WIP/'
	program = 'LB_main.py'
	dateTuple = datetime.date.today()
	dateStr = dateTuple.ctime()
	usblist = os.listdir('/media')
	print 'USB drives available',len(usblist)
	
	
	for i in range(len(usblist)):
		# copy entire directories if LB_main exists
		usbDir = '/media/'+usblist[i]+usbCD
		updatedVers = autorun(usbDir,defaultDir,program)
		if updatedVers ==0:   # there is a new version on USB
			shutil.copytree(defaultDir,'./WIP' + dateStr )
			#os.rem
			shutil.rmtree(defaultDir)
			print 'deleted ', defaultDir
			shutil.copytree(usbDir,defaultDir)
			break
		os.system('sudo chown $USER -R ../')
		
		#copy    /media ... /LightPainter/Images if it exists
		try: 
			imagelist = os.listdir(usbDir+'Images/')
			if imagelist: # if there are images in /media/LightPainter/Images
				shutil.copytree(usbDir+'Images/', defaultDir+'Images/') 
		os.system('sudo chown $USER -R ../')		
	
	
	
	os.system('sudo chown $USER -R ../')
	os.system('sudo chown $USER -R /home/pi/Desktop/LightPainter/')
	os.system('sudo python '+ defaultDir+program)
