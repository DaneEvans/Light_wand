/*

  Heavily modified in 2020 by Dane Evans 
  - modified for a nano - it is short on space though 
  - timer driven 
  - live menu selection 
  - digital menu navigation 
  - continuous modes 

  Theres a lot of bloat and globals in here, which are limiting the length of the files and led string i can drive 
  some image files don't work (170kB ), max string length is 89, I have a feeling I have also limited the number of files that can be read 

  nano has 2k ram 
  micro has 2.5k 
  the ble nano is on an nrf52840 and would have heaps of space, and potentially a btle app
  

  
  
  
  Digital Light Wand + SD + LCD + Arduino MEGA - V MRR-3.0 (WS2812 RGB LED Light Strip)
  by Michael Ross 2014
  Based on original code by myself in 2010 then enhanced by Is0-Mick in 2012

  The Digital Light Wand is for use in specialized Light Painting Photography
  Applications.

  This code is totally rewritten using code that IsO-Mick created made to specifically
  support the WS2812 RGB LED strips running with an SD Card, an LCD Display, and the
  Arduino Mega 2560 Microprocessor board.

  The functionality that is included in this code is as follows:

  Menu System
  1 - File select
  2 - Brightness
  3 - Initial Delay
  4 - Frame Delay
  5 - Repeat Times The number of times to repeat the current file playback)
  6 - Repeat Delay (if you want a delay between repeated files)

  This code supports direct reading of a 24bit Windows BMP from the SD card.
  BMP images must be rotated 90 degrees clockwise and the width of the image should match the
  number of pixels you have on your LED strip.  The bottom of the tool will be the INPUT
  end of the strips where the Arduino is connected and will be the left side of the input
  BMP image.

  Mick also added a Gamma Table from adafruit code which gives better conversion of 24 bit to
  21 bit coloring.

*/

// Library initialization
#include <Adafruit_NeoPixel.h>           // Library for the WS2812 Neopixel Strip
#include <SD.h>                          // Library for the SD Card
#include <SPI.h>                         // Library for the SPI Interface
#include <Wire.h>

// Pin assignments for the Arduino (Make changes to these if you use different Pins)
#define SDssPin 10                        // SD card CS pin
#define NPPin 6                           // Data Pin for the NeoPixel LED Strip
//#define AuxButton 2                       // Aux Select Button Pin
byte g = 0;                                // Variable for the Green Value
byte b = 0;                                // Variable for the Blue Value
byte r = 0;                                // Variable for the Red Value

// Intial Variable declarations and assignments (Make changes to these if you want to change defaults)
#define STRIP_LENGTH 89                  // Set the number of LEDs the LED Strip

//ranges
byte bright_options[] = {50, 100, 150, 200, 10, 20, 30}; // seems to max out at < 245
byte bright_index = 0;
byte brightness = bright_options[bright_index];                      // Variable and default for the Brightness of the strip

byte sc_speed_options[] = {0, 5, 10, 20, 40, 100, 200};
byte sc_speed_index = 0;
byte frameDelay = sc_speed_options[sc_speed_index];                    // default for the frame delay / scroll speed

byte repeat_delay_options[] = {0, 100, 200};          // *10 ms
byte repeat_delay_index = 0;
byte repeatDelay = repeat_delay_options[repeat_delay_index];                      // Variable for delay between repeats (default continuous)

//byte repeat = 0;                           // Variable to select auto repeat (until select button is pressed again)

//timer
unsigned long last_timer;
//timer counters
byte timer_count = 0;  // rolls over at 50ms
int inter_frame_count = 0 ;
byte inter_row_count = 0 ;


// globals for image data in non-blocking methods
byte displayWidth;
byte displayHeight;
uint32_t lineLength;
byte currentRow ;
bool image_progress_flag = false;

Adafruit_NeoPixel strip = Adafruit_NeoPixel(STRIP_LENGTH, NPPin, NEO_GRB + NEO_KHZ800);

// Variable assignments for the Keypad
#define NUM_KEYS 5

#define BTN_UP 8    // bright
#define BTN_DOWN 7   // scroll speed 
#define BTN_LEFT 2   // interframe
#define BTN_RIGHT 3  // next file 
#define BTN_SEL 5   // pause 


enum {
  key_up = 1,
  key_down,
  key_left,
  key_right,
  key_sel,
  key_none = 255
};

byte key = -1;

// SD Card Variables and assignments
File root;
File dataFile;
String m_CurrentFilename = "";
byte m_FileIndex = 0;
byte m_NumberOfFiles = 0;
String m_FileNames[10];  // this has been reduced from 200 to find some space

char currentFile[14];

// ===============================
// ====     initialisation    ====
// ===============================

// Setup loop to get everything ready.  This is only run once at power on or reset
void setup() {
  setupComms();
  setupLEDs();
  setupSDcard();
  setupButtons();
  image_progress_flag = false;

  Serial.println(F("Setup complete"));
}

// setup comms, in this case serial
void setupComms() {
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.println(F("Serial inited"));
}

void setupLEDs() {
  strip.begin();
  strip.setPixelColor(0, strip.Color(15, 0, 0)); // set first pixel to a dull red 
  strip.show();
}

void setupSDcard() {
  pinMode(SDssPin, OUTPUT);
  while (!SD.begin(SDssPin)) {
    Serial.println(F("SD init failed! "));
  }
  Serial.println(F("SD init done.   "));
  delay(1000);
  root = SD.open("/");
  Serial.println(F("Scanning files  "));

  GetFileNamesFromSD(root);
  isort(m_FileNames, m_NumberOfFiles);
  m_CurrentFilename = m_FileNames[0];
  Serial.println(F("Current File: "));
  DisplayCurrentFilename();

  Serial.println(F("Scanning files completed "));
  strip.setPixelColor(0, strip.Color(0, 15, 0)); // set first pixel to a dull green 
  strip.show();
}

// setup buttons
void setupButtons() {
  pinMode(BTN_UP, INPUT_PULLUP);
  pinMode(BTN_DOWN, INPUT_PULLUP);
  pinMode(BTN_LEFT, INPUT_PULLUP);
  pinMode(BTN_RIGHT, INPUT_PULLUP);
  pinMode(BTN_SEL, INPUT_PULLUP);

  key = key_none;
}

// =================================================
// ====                 loop                    ====
// =================================================
// The Main Loop for the program starts here...
// This will loop endlessly looking for a key press to perform a function
bool key_ready = true;

void loop() {
  unsigned long timeis =  millis();

  if ((timeis - last_timer) >= (5)) { // 5ms timer
    last_timer = timeis;
    timer_count ++ ;

    // 50ms
    if (timer_count >= 10) {
      timer_count = 0;
      key = ReadKeypad();
      if ((key != key_none) & (key_ready == true)) {
        key_ready = false;
        handle_keypress(key);
      }
      else if (key == key_none)
      {
        key_ready = true;
      }
    }

    if (image_progress_flag) {
      if (inter_row_count == 0) {  // ready for a new row
        inter_row_count = (frameDelay / 5) + 1;
        displayNextRow();
      }
      inter_row_count-- ;
    }

    // next image to display
    else if (inter_frame_count == 0 ) {
      dataFile.close();
      // send out debug data
      displayData();
      // send to lights
      SendFile(m_CurrentFilename);
    }
    inter_frame_count--;
  }
}

// =================================================
// ====              Read Keys                  ====
// =================================================

int ReadKeypad() {
  key = readKeyIOs();
  if (key != key_none) {                    // if keypress is detected
    delay(5);                            // wait for debounce time
    key = readKeyIOs();     // then read the inputs again
    if (key >= 0) {
      return key;
    }
  }
  return key_none;
}

int readKeyIOs() {
  key = key_none;
  if (!digitalRead(BTN_UP)) {
    key = key_up;
  }
  if (!digitalRead(BTN_DOWN)) {
    key = key_down;
  }
  if (!digitalRead(BTN_LEFT)) {
    key = key_left;
  }
  if (!digitalRead(BTN_RIGHT)) {
    key = key_right;
  }
  if (!digitalRead(BTN_SEL)) {
    key = key_sel;
  }
  return key;
}

void handle_keypress(byte key) {
  switch (key) {
    case key_right:
      //Serial.println(F("next file"));
      if (m_FileIndex < m_NumberOfFiles - 1) {
        m_FileIndex++;
      }
      else {
        m_FileIndex = 0;                // On the last file so wrap round to the first file
      }
      DisplayCurrentFilename();
      delay(50);
      currentRow = 0 ;
      image_progress_flag = false;
      inter_frame_count = 1 ;
      //Serial.println(m_FileIndex);
      break;


    case key_up: // this one works
      bright_index = (bright_index + 1) % (sizeof(bright_options) / sizeof(bright_options[0])) ;
      brightness = bright_options[bright_index];
      //Serial.print(F("brightness "));
      //Serial.println(brightness);
      break;

    case key_down:  // scroll speed
      sc_speed_index = (sc_speed_index + 1) % (sizeof(sc_speed_options) / sizeof(sc_speed_options[0]));
      frameDelay = sc_speed_options[sc_speed_index];
      //Serial.print(F("sc_speed "));
      //Serial.println(frameDelay);
      break;

    case key_left:  // repeat delay
      repeat_delay_index = (repeat_delay_index + 1) % (sizeof(repeat_delay_options) / sizeof(repeat_delay_options[0]));
      repeatDelay = repeat_delay_options[repeat_delay_index];
      //Serial.println(F("repeat_delay "));
      //Serial.println(repeatDelay);
      break;

    case key_sel: // pause
      bool pause = true ;
      bool hold = false ;
      bool start = true ;
      Serial.println(F("Paused  "));
      delay(150); // time to release button
      while (pause) {
        key = ReadKeypad();
        switch (key) {
          case key_sel: // pause still held
            hold = true ;
            break;
          case (key_none):
          default:
            if (hold) {
              pause = false;
            }
            break;
        }
      }
      if (!image_progress_flag) // if in space between images, start immediately
      {
        inter_frame_count = 10; 
      }
      break;

    default:
      break;
      Serial.println(F("unsupported key"));
  }
}


// =================================================
// ====              LEDs                       ====
// =================================================


//  send the file to the leds
void SendFile(String Filename) {
  Filename.toCharArray(currentFile, 14);

  dataFile = SD.open(currentFile);

  // if the file is available send it to the LED's
  if (dataFile) {
    ReadTheFileInfo();
    //dataFile.close();
  }
  else {
    Serial.print(F("  Error reading file "));
    delay(1000);
    setupSDcard();
    return;
  }
}

void ClearStrip() {
  int x;
  for (x = 0; x < STRIP_LENGTH; x++) {
    strip.setPixelColor(x, 0);
  }
  strip.show();
}

void getRGBwithGamma() {
  g = gamma(readByte()) / (101 - brightness);
  b = gamma(readByte()) / (101 - brightness);
  r = gamma(readByte()) / (101 - brightness);
}

PROGMEM const unsigned char gammaTable[]  = {
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,
  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  2,  2,  2,  2,
  2,  2,  2,  2,  2,  3,  3,  3,  3,  3,  3,  3,  3,  4,  4,  4,
  4,  4,  4,  4,  5,  5,  5,  5,  5,  6,  6,  6,  6,  6,  7,  7,
  7,  7,  7,  8,  8,  8,  8,  9,  9,  9,  9, 10, 10, 10, 10, 11,
  11, 11, 12, 12, 12, 13, 13, 13, 13, 14, 14, 14, 15, 15, 16, 16,
  16, 17, 17, 17, 18, 18, 18, 19, 19, 20, 20, 21, 21, 21, 22, 22,
  23, 23, 24, 24, 24, 25, 25, 26, 26, 27, 27, 28, 28, 29, 29, 30,
  30, 31, 32, 32, 33, 33, 34, 34, 35, 35, 36, 37, 37, 38, 38, 39,
  40, 40, 41, 41, 42, 43, 43, 44, 45, 45, 46, 47, 47, 48, 49, 50,
  50, 51, 52, 52, 53, 54, 55, 55, 56, 57, 58, 58, 59, 60, 61, 62,
  62, 63, 64, 65, 66, 67, 67, 68, 69, 70, 71, 72, 73, 74, 74, 75,
  76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91,
  92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 104, 105, 106, 107, 108,
  109, 110, 111, 113, 114, 115, 116, 117, 118, 120, 121, 122, 123, 125, 126, 127
};


inline byte gamma(byte x) {
  return pgm_read_byte(&gammaTable[x]);
}


// =================================================
// ====             Messaging                   ====
// =================================================

//print out current data - debug
void displayData()
{
  Serial.println(F(""));
  Serial.println(m_CurrentFilename);
  Serial.print(F("brightness "));
  Serial.println(brightness);
  //Serial.println(initDelay);
  Serial.print(F("frameDelay "));
  Serial.println(frameDelay);
  //Serial.println(repeatTimes);
  Serial.print(F("repeatDelay "));
  Serial.println(repeatDelay);
}


void DisplayCurrentFilename() {
  m_CurrentFilename = m_FileNames[m_FileIndex];
  Serial.println(F(""));
  Serial.println(m_CurrentFilename);
}


// =================================================
// ====               SD Card                   ====
// =================================================
#define MYBMP_BF_TYPE           0x4D42
#define MYBMP_BF_OFF_BITS       54
#define MYBMP_BI_SIZE           40
#define MYBMP_BI_RGB            0L
#define MYBMP_BI_RLE8           1L
#define MYBMP_BI_RLE4           2L
#define MYBMP_BI_BITFIELDS      3L

void GetFileNamesFromSD(File dir) {
  int fileCount = 0;
  String CurrentFilename = "";
  while (1) {
    File entry =  dir.openNextFile();
    //Serial.println(F("getting files"));
    if (! entry) {
      //Serial.println(F("no more files"));
      // no more files
      m_NumberOfFiles = fileCount;
      entry.close();
      break;
    }
    else {
      //Serial.println(F("more files"));
      if (entry.isDirectory()) {
        //Serial.println(F("is dir"));
        //GetNextFileName(root);
      }
      else {
        CurrentFilename = entry.name();
        //Serial.println("incurrfile");
        //Serial.println(CurrentFilename);
        if (CurrentFilename.endsWith(".bmp") || CurrentFilename.endsWith(".BMP") ) { //find files with our extension only
          m_FileNames[fileCount] = entry.name();
          fileCount++;
        }
      }
    }
    entry.close();
  }
}

void ReadTheFileInfo() {
#define MYBMP_BF_TYPE           0x4D42
#define MYBMP_BF_OFF_BITS       54
#define MYBMP_BI_SIZE           40
#define MYBMP_BI_RGB            0L
#define MYBMP_BI_RLE8           1L
#define MYBMP_BI_RLE4           2L
#define MYBMP_BI_BITFIELDS      3L

  uint16_t bmpType = readInt();
  uint32_t bmpSize = readLong();
  uint16_t bmpReserved1 = readInt();
  uint16_t bmpReserved2 = readInt();
  //uint32_t bmpOffBits = readLong();
  // no need to keep the data
  readLong();



  // Check file header
  if (bmpType != MYBMP_BF_TYPE) {
    Serial.print(F("RTFI not a bitmap"));
    delay(1000);
    return;
  }

  // Read info header
  uint32_t imgSize = readLong();
  uint32_t imgWidth = readLong();
  uint32_t imgHeight = readLong();
  uint16_t imgPlanes = readInt();
  uint16_t imgBitCount = readInt();
  uint32_t imgCompression = readLong();
  uint32_t imgSizeImage = readLong();
  uint32_t imgXPelsPerMeter = readLong();
  uint32_t imgYPelsPerMeter = readLong();
  uint32_t imgClrUsed = readLong();
  uint32_t imgClrImportant = readLong();

  // Check info header
  if ( imgSize != MYBMP_BI_SIZE || imgWidth <= 0 ||
       imgHeight <= 0 || imgPlanes != 1 ||
       imgBitCount != 24 || imgCompression != MYBMP_BI_RGB ||
       imgSizeImage == 0 )
  {
    Serial.println(F("Unsupported"));
    Serial.println(F("Bitmap Use 24bpp"));
    delay(1000);
    return;
  }

  displayHeight = imgHeight;
  displayWidth = imgWidth;
  if (imgWidth > STRIP_LENGTH) {
    displayWidth = STRIP_LENGTH;           //only display the number of led's we have
  }

  // compute the line length
  lineLength = imgWidth * 3;
  if ((lineLength % 4) != 0)
    lineLength = (lineLength / 4 + 1) * 4;

  currentRow = displayHeight;
  image_progress_flag = true;
}

void displayNextRow() {
  // image done, or not in progress
  if (!image_progress_flag) {
    return;
  }
  else if (currentRow == 0 && image_progress_flag) {
    if (repeatDelay != 0 )
    {
      image_progress_flag = false;
      ClearStrip();
      inter_frame_count = (repeatDelay * 10  / 5 ) + 1;
      dataFile.close();
    }
    else
    {
      //reset row counter 
      currentRow = displayHeight;
    }
  }
  else if (currentRow > 0) {
    //dataFile = SD.open(currentFile);

    // if the file is available send it to the LED's
    if (dataFile) {
      for (int x = 0; x < displayWidth; x++) {
        uint32_t offset = (MYBMP_BF_OFF_BITS + (((currentRow - 1) * lineLength) + (x * 3))) ;
        dataFile.seek(offset);
        getRGBwithGamma();
        strip.setPixelColor(x, r, b, g);
      }
      currentRow -- ;
      strip.show();
    }
    //dataFile.close();
  }
  else {
    Serial.print(F("  Error reading file "));
    delay(1000);
    setupSDcard();
    return;
  }
}


// Sort the filenames in alphabetical order
void isort(String *filenames, int n) {
  for (int i = 1; i < n; ++i) {
    String j = filenames[i];
    int k;
    for (k = i - 1; (k >= 0) && (j < filenames[k]); k--) {
      filenames[k + 1] = filenames[k];
    }
    filenames[k + 1] = j;
  }
}


// =================================================
// ====             Image reading               ====
// =================================================

uint32_t readLong() {
  uint32_t retValue;
  byte incomingbyte;

  incomingbyte = readByte();
  retValue = (uint32_t)((byte)incomingbyte);

  incomingbyte = readByte();
  retValue += (uint32_t)((byte)incomingbyte) << 8;

  incomingbyte = readByte();
  retValue += (uint32_t)((byte)incomingbyte) << 16;

  incomingbyte = readByte();
  retValue += (uint32_t)((byte)incomingbyte) << 24;

  return retValue;
}

uint16_t readInt() {
  byte incomingbyte;
  uint16_t retValue;

  incomingbyte = readByte();
  retValue += (uint16_t)((byte)incomingbyte);

  incomingbyte = readByte();
  retValue += (uint16_t)((byte)incomingbyte) << 8;

  return retValue;
}

int readByte() {
  int retbyte = -1;
  while (retbyte < 0) retbyte = dataFile.read();
  return retbyte;
}
